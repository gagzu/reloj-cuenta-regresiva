!((d) => {
	d.addEventListener('DOMContentLoaded', () => {
		/**
		 * [getRemainTime description]
		 * @param  {array[string | number]} deadline Tiempo límite
		 * recibe un array con una fecha
		 * en string con el formato correcto o un array de numeros
		 * getRemainTime([año_num,mes_num,dia_num ,hor_num,min_num,seg_num,mils_num])
		 * getRemainTime(['Fri Sep 11 2020 00:00:00 GMT-0300'])
		 * @return {object}
		 */
		const getRemainTime = (deadline) => {
			let
				now = new Date(),
				remainTime = (new Date(...deadline) - now + 1000) / 1000,
				remainSeconds = ('0' + Math.floor(remainTime % 60)).slice(-2),
				remainMinutes = ('0' + Math.floor(remainTime / 60 % 60)).slice(-2),
				remainHours = ('0' + Math.floor(remainTime / 3600 % 24)).slice(-2),
				remainDays = Math.floor(remainTime / (3600 * 24));

			return {
				remainTime,
				remainSeconds,
				remainMinutes,
				remainHours,
				remainDays
			}
		}

		/**
		 * [countdown description]
		 * @param  {array} $deadline Tiempo límite
		 * @param  {string} $targetId ID del nodo que contiene el countdown
		 * @return void
		 */
		const countdown = (deadline, targetId) => {
			const el = d.getElementById(targetId);

			const timerUpdate = setInterval(() => {
				let t = getRemainTime(deadline)

				if(t.remainTime <= 1) {
					/* tiempo finalizado - detener el countdown */
					clearInterval(timerUpdate);
				} else {
					// Cambiar el DOM solo si aún queda tiempo restante
					el.querySelector('.dias .value').innerHTML = t.remainDays;
					el.querySelector('.horas .value').innerHTML = t.remainHours;
					el.querySelector('.minutos .value').innerHTML = t.remainMinutes;
					el.querySelector('.segundos .value').innerHTML = t.remainSeconds;
				}
			}, 1000)
		}

		/* Ejemplo de como iniciar la cuenta regresiva */
		if(false) {
			let deadline = [
				2020, // Año
				7, // mes (enero = 0)
				18, // Día
				17, // Minutos
				17, // Segundos
				0 // Milisegundos
			];
			countdown(deadline, 'countdown');
		}

		/* Iniciar la cuenta regresiva para que siempre demore 1 Día */
		countdown([Date.now() + 1000 * (3600 * 24)], 'countdown');
	})
})(document);