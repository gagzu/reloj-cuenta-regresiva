# RELOJ DE CUENTA REGRESIVA #

### ¿Para qué es este repositorio? ###

Básicamente es una landingPage con un cronometro que puede ser utilizado para nuevos lanzamientos, promociones o cualquier otro uso que se les pueda ocurrir

### ¿Cómo configurar? ###

No requiere ninguna dependencia ni configuración compleja ya que está hecho con puro HTML, CSS y JS

### DEMO ###

Disponible en => [https://demos.gallbers.uy/countdown](https://demos.gallbers.uy/countdown)